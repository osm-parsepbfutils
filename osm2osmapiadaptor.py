import osm
import OsmApi
from parsepbf import PBFParser

def copySpecificToOsmApi(objtype, toobj, fromobj):

    if objtype == u'node':
        toobj[u'lat'] = fromobj.Lat
        toobj[u'lon'] = fromobj.Lon
        toobj[u'id']  = fromobj.NodeID
        toobj[u'visible'] = fromobj.Tags.get(u'visible',True)
    elif objtype == u'way':
        toobj[u'nd'] = fromobj.Nds
        toobj[u'id']  = fromobj.WayID
    elif objtype == u'relation':
        toobj[u'id']  = fromobj.RelID
        #l = []
        #for x in fromobj.Members:
        #    l.append({u'ref':x.ref, u'role':x.role, u'type':x.type})
        l = [ {u'ref':x.ref, u'role':x.role, u'type':x.type} for x in fromobj.Members]
        toobj['member']=l

def toOsmApiFormat(osm):

    if not isinstance(osm, PBFParser):
        raise ValueError

    osmApiData = []

    ls = {}
    ls[u'node']=osm.Nodes
    ls[u'way']=osm.Ways
    ls[u'relation']=osm.Relations

    for t in (u'node', u'way', u'relation'):
        for n in ls[t]:
            on = {}
            on = {u'uid':n.uid,
                  u'changeset':n.changeset,
                  u'version':n.version,
                  u'user':n.user,
                  u'timestamp':n.time,
                  u'tag':n.Tags}
            copySpecificToOsmApi(t, on, n)
            try:
                del on[u'tag'][u'visible']
            except KeyError:
                pass

            osmApiData.append({u'type':t, u'data':on})

    return osmApiData


